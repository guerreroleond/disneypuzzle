﻿using Microsoft.AspNetCore.Mvc;
using WordSearchPuzzleGame.BusinessLogic;
using WordSearchPuzzleGame.BusinessLogic.Contracts;

namespace WordSearchPuzzleGame.App.Controllers
{
    public class PuzzleController : Controller
    {
        private readonly IPuzzleManager _puzzleManager;

        public PuzzleController()
        {
            _puzzleManager = new PuzzleManager();
        }

        // Todo: Inject dependency
        //public PuzzleController(IPuzzleManager puzzleManager)
        //{
           
        //}

        // GET: /<controller>/
        public IActionResult Index()
        {
            var board = _puzzleManager.GetBoard();
            return View(board);
        }

       
    }
}
