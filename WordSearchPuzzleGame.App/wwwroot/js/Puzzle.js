﻿
var selectedLetters = [];

function CheckLetter(_id, _letter, _row, _col) {
    let letter = { id: _id, letter: _letter, row: parseInt(_row, 10), col: parseInt(_col,10) };
    let id = "#" + letter.id;

    if ($(id).hasClass("foundInPuzzle")) {
        return;
    }

    $(".hintWord").removeClass("hintWord");
    if ($(id).hasClass("selectedLetter")) {
        DeselectLetter(letter);
    } else {
        if (selectedLetters.length > 0) {
            let minCol = 15;
            let maxCol = 0;
            $.each(selectedLetters, function (index, tempLetter) {
                let tempRow = tempLetter.row;

                if (tempRow != _row) {
                    ClearLetters();
                    return false;
                }
                if (tempLetter.col > maxCol) {
                    maxCol = tempLetter.col;
                }
                if (tempLetter.col < minCol) {
                    minCol = tempLetter.col;
                }
            });

            if ((letter.col > (maxCol + 1)) || (letter.col < (minCol - 1))) {
                ClearLetters();
            }

            SelectLetter(letter);
        } else {
            SelectLetter(letter);
        }
    }
    if (selectedLetters.length > 3) {
        CheckWord();
    }
}

function HintWord(word, slotId) {
    let wordId = "#word_" + slotId;
    if ($(wordId).hasClass("foundInList")) {
        return;
    }
    ClearLetters();
    $(".hintWord").removeClass("hintWord");

    let initSlot = parseInt(slotId, 10);
    let wordLentgh = word.length;

    for (var i = initSlot; i < initSlot + wordLentgh; i++) {
        let id = "#" + i;
        $(id).removeClass("selectedLetter");
        $(id).addClass("hintWord");
    }
    $(wordId).addClass("hintWord");
    $(wordId).addClass("hinted");
}

function SortSelectedLetters(a, b) {
    var aCol = a.col;
    var bCol = b.col;
    return ((aCol < bCol) ? -1 : ((aCol > bCol) ? 1 : 0));
}

function CheckWord() {
    selectedLetters.sort(SortSelectedLetters);
    let word = "";
    $.each(selectedLetters, function (index, tempLetter) {
        word += tempLetter.letter;
    });

    $.each($(".notfound"), function (index, notFoundWord) {
        let tempWord = notFoundWord.innerText;

        if (tempWord == word) {
            WordFound(notFoundWord);
        }
    });
}

function WordFound(foundWord) {
    $(foundWord).addClass("foundInList");
    $(".selectedLetter").addClass("foundInPuzzle");
    ClearLetters();
    CheckBoard();
}

function CheckBoard() {
    let foundWords = $(".foundInList").length;

    if (foundWords >= 15) {
        let hintWords = $(".hinted").length;
        let score = foundWords - hintWords;
        $("#totalNoHint").html(score);
        $("#totalHint").html(hintWords);
        $("#totalScore").html(score);
        $("#results").removeClass("d-none");
    }
}

function DeselectLetter(_letter) {
    let id = "#" + _letter.id;
    $(id).removeClass("selectedLetter");

    selectedLetters = $.grep(selectedLetters, function (letter) {
        return letter.id != _letter.id;
    });
}

function SelectLetter(_letter) {
    selectedLetters.push(_letter);
    let id = "#" + _letter.id;
    $(id).addClass("selectedLetter");
}

function ClearLetters() {
    selectedLetters = [];
    $(".selectedLetter").removeClass("selectedLetter");
}

function PlayAgain() {
    location.reload();
}
