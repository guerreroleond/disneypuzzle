#pragma checksum "/Users/dguerrero/Documents/dev/Globant/Disney/WordSearchPuzzleGame/WordSearchPuzzleGame.App/Views/Home/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "31382052dc73d0874145afb3b97de6363126e602"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/dguerrero/Documents/dev/Globant/Disney/WordSearchPuzzleGame/WordSearchPuzzleGame.App/Views/_ViewImports.cshtml"
using WordSearchPuzzleGame.App;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/dguerrero/Documents/dev/Globant/Disney/WordSearchPuzzleGame/WordSearchPuzzleGame.App/Views/_ViewImports.cshtml"
using WordSearchPuzzleGame.App.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"31382052dc73d0874145afb3b97de6363126e602", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd5d3b1a2762403d413eb6ba888b07080a400297", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "/Users/dguerrero/Documents/dev/Globant/Disney/WordSearchPuzzleGame/WordSearchPuzzleGame.App/Views/Home/Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<script type=""text/javascript"">
    window.onload = function () {
        var customer = new this.Object();
        customer.firstName = ""David"";
        customer.lastName = ""Guerrero"";
        customer.shirtSize = ""M"";
        //this.sessionStorage.setItem(""cart1"", this.JSON.stringify(customer));
        LoadFromStorage();

        document.getElementById(""btnAdd"").onclick = function () {
            var key = document.getElementById(""toStorageKey"").value;
            var value = document.getElementById(""toStorageValue"").value;
            alert('adding ' + key + ' ' + value);
            sessionStorage.setItem(document.getElementById(""toStorageKey"").value,
                                    document.getElementById(""toStorageValue"").value);
            LoadFromStorage();
        }
        document.getElementById(""btnRemove"").onclick = function () {
            sessionStorage.removeItem(document.getElementById(""toStorageKey"").value);
            LoadFromStorage();
        }
        document.getElementById(""");
            WriteLiteral(@"btnClear"").onclick = function () {
            sessionStorage.clear();
            LoadFromStorage();
        }
        function LoadFromStorage() {
            var storageDiv = document.getElementById(""storage"");
            var tbl = document.createElement(""table"");
            tbl.id = ""storageTable"";

            //alert('elements in storage: ' + sessionStorage.length);

            if (sessionStorage.length > 0) {
                for (var i = 0; i < sessionStorage.length; i++) {
                    var row = document.createElement(""tr"");
                    var key = document.createElement(""td"");
                    var val = document.createElement(""td"");
                    key.innerText = sessionStorage.key(i);
                    val.innerText = sessionStorage.getItem(key.innerText);
                    row.appendChild(key);
                    row.appendChild(val);
                    tbl.appendChild(row);
                }
            }
            else {
                var row = document.createEle");
            WriteLiteral(@"ment(""tr"");
                var col = document.createElement(""td"");
                col.innerText = ""No data in local storage."";
                row.appendChild(col);
                tbl.appendChild(row);
            }

            if (document.getElementById(""storageTable"")) {
                document.getElementById(""storageTable"").replaceNode(tbl);
            }
            else {
                storageDiv.appendChild(tbl);
            }
        }
    }
</script>

<section>
    Key:
    <input type=""text"" id=""toStorageKey"" />

    Value:
    <input type=""text"" id=""toStorageValue"" /><br/>
</section>
<section>
    <button type=""button"" id=""btnAdd"">Add To Storage</button>
    <button type=""button"" id=""btnRemove"">Remove From Storage</button>
    <button type=""button"" id=""btnClear"">Clear Storage</button>
</section>
<div id=""storage"">
    <p>Current Storage Contents</p>
</div>

<div class=""text-center"">
    <h1 class=""display-4"">Welcome</h1>
    <p>Learn about <a href=""https://docs.microsoft.com/aspnet/core"">b");
            WriteLiteral("uilding Web apps with ASP.NET Core</a>.</p>\n</div>\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
