﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordSearchPuzzleGame.BusinessLogic.AppConfig;
using WordSearchPuzzleGame.BusinessLogic.Contracts;
using WordSearchPuzzleGame.Common.Models;

namespace WordSearchPuzzleGame.BusinessLogic
{
    public class PuzzleManager : IPuzzleManager
    {
        private Board _board;
        private List<Word> _searchingWords;
        private List<string> _alphabet;
        private AppConfiguration _appConfiguration;

        public PuzzleManager()
        {
            _appConfiguration = new AppConfiguration();
        }

        public Board GetBoard()
        {
            _board = new Board();
            _searchingWords = GetSearchingWords().ToList();
            _alphabet = GetAlphabet().ToList();
            PlaceWordsInBoard();
            var slotsWithLetter = _board.Slots.Where(s => s.HasAWordLetter).ToList();
            FillEmptySlots();
            return _board;
        }

        private void FillEmptySlots()
        {
            var emptySlots = _board.Slots.Where(s => s.IsEmpty).ToList();

            foreach (var slot in emptySlots) FillEmptySlot(slot);
        }

        private void FillEmptySlot(Slot emptySlot)
        {
            var random = new Random();
            var randomIndex = random.Next(0, _alphabet.Count - 1);
            _board.Slots.FirstOrDefault(s =>
                    s.Id == emptySlot.Id).Letter = _alphabet[randomIndex];
            _board.Slots.FirstOrDefault(s =>
                    s.Id == emptySlot.Id).IsEmpty = false;
        }

        private void PlaceWordsInBoard()
        {
            for (int row = 1; row <= _board.Rows; row++)
            {// Place a word in every row
                var wordToPlace = PickAWord();
                if (wordToPlace != null)
                {
                    var random = new Random();
                    // Chose an index to place the word depending on its length
                    var indexToPlace = random.Next(1, _board.Cols - wordToPlace.Text.Length);
                    PlaceAWordInBoard(row, indexToPlace, wordToPlace);
                    _board.Words.Add(wordToPlace);
                }
            }
        }

        private void PlaceAWordInBoard(int row, int col, Word wordToPlace)
        {
            for (int i = 0; i < wordToPlace.Text.Length; i++)
            {
                var slot = _board.Slots.FirstOrDefault(s => s.Row == row && s.Col == col);
                if (slot != null)
                {
                    if(i == 0)
                        wordToPlace.SlotId = slot.Id;

                    _board.Slots.FirstOrDefault(s => s.Row == row  && s.Col == col)
                        .Letter = wordToPlace.Text[i].ToString().ToUpper();
                    _board.Slots.FirstOrDefault(s => s.Row == row && s.Col == col)
                        .IsEmpty = false;
                    _board.Slots.FirstOrDefault(s => s.Row == row && s.Col == col)
                        .HasAWordLetter = true;
                    col++;
                }
            }
        }

        private Word PickAWord()
        {
            var isAWordPicked = false;
            var random = new Random();

            while (!isAWordPicked)
            {
                var randomIndex = random.Next(0, _searchingWords.Count - 1);
                var pickedWord = _searchingWords[randomIndex];

                if (!pickedWord.IsOnBoard)
                {
                    isAWordPicked = true;
                    pickedWord.IsOnBoard = true;
                    return pickedWord;
                }
            }

            return null;
        }

        private IList<Word> GetSearchingWords()
        {
            var searchingWords = new List<Word>();

            _appConfiguration.SearchingWords.ToList().ForEach(w =>
            {
                searchingWords.Add(new Word { Text = w.ToUpper(), Found = false });
            });

            return searchingWords;
        }

        private IList<string> GetAlphabet()
        {
            var alphabet = new List<string>();

            _appConfiguration.Alphabet.ToList().ForEach(l =>
            {
                alphabet.Add(l);
            });

            return alphabet;
        }
    }
}
