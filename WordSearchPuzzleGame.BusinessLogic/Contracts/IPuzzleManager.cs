﻿using WordSearchPuzzleGame.Common.Models;

namespace WordSearchPuzzleGame.BusinessLogic.Contracts
{
    public interface IPuzzleManager
    {
        Board GetBoard();
    }
}
