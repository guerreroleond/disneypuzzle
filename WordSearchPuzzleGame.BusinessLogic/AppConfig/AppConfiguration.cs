﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace WordSearchPuzzleGame.BusinessLogic.AppConfig
{ 
    public class AppConfiguration
    {
        private readonly string _searchingWordsString;
        private readonly string _aphabetString;

        public AppConfiguration()
        {
            var configBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configBuilder.AddJsonFile(path, false);

            var root = configBuilder.Build();
            var searchingWords = root.GetSection("Words");
            var alphabet = root.GetSection("Alphabet");
            _searchingWordsString = searchingWords.Value;
            _aphabetString = alphabet.Value;
        }

        public string[] SearchingWords
        {
            get => _searchingWordsString.Split(",");
        }

        public string[] Alphabet
        {
            get => _aphabetString.Split(",");
        }
    }
}
