﻿using System;
using NUnit.Framework;
using WordSearchPuzzleGame.BusinessLogic;
using WordSearchPuzzleGame.BusinessLogic.Contracts;
using WordSearchPuzzleGame.Common.Models;

namespace WordSerachPuzzleGame.Tests
{
    [TestFixture]
    public class PuzzleManagerTests
    {
        private IPuzzleManager _puzzleManager;
        private Board _board;

        [SetUp]
        public void SetUp()
        {
            _puzzleManager = new PuzzleManager();
            _board = _puzzleManager.GetBoard();
        }

        [Test]
        public void PuzzleWordsToFind_BustBeAtLeastThree()
        {
            var puzzleWordsToFind = _board.Words.Count;

            Assert.GreaterOrEqual(puzzleWordsToFind, 3);
        }

        [Test]
        public void PuzzleWords_MustBeThreeOrMoreLettersLong()
        {
            foreach (var word in _board.Words)
            {
                var wordLettersLong = word.Text.Length;
                Assert.GreaterOrEqual(wordLettersLong, 3);
            }
        }

        [Test]
        public void PuzzleSlots_MustBeEqualsThanRowsTimesCols()
        {
            var rowsTimesCols = _board.Rows * _board.Cols;
            var puzzleSlots = _board.Slots.Count;

            Assert.AreEqual(rowsTimesCols, puzzleSlots);
        }
    }
}
