﻿using System;
namespace WordSearchPuzzleGame.Common.Models
{
    public class Slot
    {
        public int Id { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public bool IsEmpty { get; set; }
        public bool HasAWordLetter { get; set; }
        public string Letter { get; set; }
        public bool IsPressed { get; set; }

        public Slot()
        {
        }

        public Slot(int row, int col, bool isEmpty, string letter, bool hasAWordLetter, bool isPressed)
        {
            Row = row;
            Col = col;
            IsEmpty = isEmpty;
            Letter = letter;
            HasAWordLetter = hasAWordLetter;
            IsPressed = isPressed;
        }
    }
}
