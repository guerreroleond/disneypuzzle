﻿using System;
namespace WordSearchPuzzleGame.Common.Models
{
    public class Word
    {
        public string Text { get; set; }

        // ToDo: change for an status enum and include HintFound
        public bool Found { get; set; }

        public bool IsOnBoard { get; set; }

        public int SlotId { get; set; }

        public Word()
        {
        }
    }
}
