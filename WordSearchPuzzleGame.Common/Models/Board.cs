﻿using System.Collections.Generic;

namespace WordSearchPuzzleGame.Common.Models
{
    public class Board
    {
        private const int c_rows = 15;
        private const int c_cols = 15;

        public int Rows { get; set; }
        public int Cols { get; set; }
        public IList<Slot> Slots { get; set; }
        public IList<Word> Words { get; set; }

        public Board()
        {
            Rows = c_rows;
            Cols = c_cols;
            Words = new List<Word>();
            LoadEmptyBoard();
        }

        private void LoadEmptyBoard()
        {
            Slots = new List<Slot>();
            var incremental = 1;

            for (int row = 1; row <= c_rows; row++)
            {
                for (int col = 1; col <= c_cols; col++)
                {
                    var slot = new Slot
                    { // not in base zero
                        Id = incremental,
                        Row = row,
                        Col = col,
                        IsEmpty = true
                    };

                    Slots.Add(slot);
                    incremental++;
                }
            }
        }
    }
}
